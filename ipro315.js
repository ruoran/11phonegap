mutex = 0

function onLoad() {		
    document.addEventListener("deviceready", onDeviceReady, false);
}

function populateDB(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS MENU (name unique, type, price)');
}

// Query the database
//
function queryDB(tx) {
    tx.executeSql('SELECT * FROM MENU', [], querySuccess, errorCB);
}

// Query the success callback
//
function querySuccess(tx, results) {
    var len = results.rows.length;
    $("#w1").text("MENU table: " + len + " rows found.");
    
    alert("query good");
}

// Transaction error callback
//
function errorCB(err) {
    console.log("Error processing SQL: "+err.code);
}

// Transaction success callback
//
function successCB() {
    alert("iiThru ready");
}

// PhoneGap is ready
//

function query_db() {
	var db = window.openDatabase("Database", "1.0", "ipro315", 200000);
    db.transaction(queryDB, errorCB);
}

// device_info	
function device_info() {
	var element = document.getElementById('deviceProperties');
	element.innerHTML = 'Device Name: '     + device.name     + '<br />' + 
            'Device PhoneGap: ' + device.phonegap + '<br />' + 
            'Device Platform: ' + device.platform + '<br />' + 
            'Device UUID: '     + device.uuid     + '<br />' + 
            'Device Version: '  + device.version  + '<br />';                          
}

function onDeviceReady() {
	$.mobile.page.prototype.options.domCache = false;
	
    var db = window.openDatabase("Database", "1.0", "ipro315", 200000);
    db.transaction(populateDB, errorCB, successCB);
    
 	//$("#get_db").bind("tap", query_db);

	$("#device_info").bind("tap", device_info);
	
	$("#test").bind("tap", function() {alert("tap");checkConnection();} );
                          
    $("#get_menu").bind("tap", get_menu);
    
    $("#plus").bind("vclick", function() {
    	var qty = $("#item_qty").text();
    	if (mutex == 0)
    	{
    		mutex = 1;
    		/* do something here */
    		$("#item_qty").text(parseInt(qty)+1);
    		setTimeout("mutex = 0",400)
    	}
    });
    
    $("#minus").bind("tap", function(){
    	var qty = $("#item_qty").text();
    	if (qty == 0)
    	{
    		return false;
    	}
    	
    	if (mutex == 0)
    	{
    		mutex = 1;
    		/* do something here */
    		$("#item_qty").text(parseInt(qty)-1);
    		setTimeout("mutex = 0",400)
    	}	
    } );
    
    //$("#menuPage").live("pageshow", show_menu);
    show_menu();

    $("#bagPage").live("pageshow", show_bag);
    $('#submit_order').bind('tap', submit_order);
    $('#pass_submit_order').bind('tap', pass_submit_order);

    
    //Bluetooth 
    //$('#get-discoverable-devices').bind('tap', getDevicesEvent);
}

function checkConnection() {
	var flag = navigator.network.connection.type != null;
    alert(flag);
}

function get_menu() {
	var db = window.openDatabase("Database", "1.0", "ipro315", 200000);
	
	if (mutex == 0)
	{    	
		mutex = 1;  	
		$.getJSON('http://ipro315.dyndns.org/menu_m', function(data) {
	    	var val1;
	    	var val2;
	    	alert("menu updated, please restart app.")
	    	db.transaction(function(tx){
	    		tx.executeSql('DROP TABLE IF EXISTS MENU');
	    		tx.executeSql('CREATE TABLE IF NOT EXISTS MENU (name unique, type, price)');
	    	});
	    	
	    	db.transaction(function(tx){
	    		$.each(data, function(key, val) {
		        	val1 = val.split(",")[0];
		        	val2 = val.split(",")[1];
		        	tx.executeSql('INSERT INTO MENU (name, type, price) VALUES (?,?,?)', [key, val1, val2], menuUpdateSuccess, menuUpdateError);
	    		});
	    	});  
		});
		setTimeout("mutex = 0",500);
    }
	
	
}

function menuUpdateSuccess(tx, results) {    
}

function menuUpdateError(err) {
    alert("Update Menu SQL Error: "+err.code);
}

function show_menu() {
	var db = window.openDatabase("Database", "1.0", "ipro315", 200000);
	db.transaction(function(tx){
		tx.executeSql('SELECT name,price,type FROM MENU ORDER BY type', [], showMenuSuccess, errorCB);
	});
}


function showMenuSuccess(tx, results) {
	//var items = [];
	var the_type = "";
	var len = results.rows.length;
    for (var i=0; i<len; i++){
    	//items.push("<li>" + results.rows.item(i).name + " " + results.rows.item(i).type + " -- " + results.rows.item(i).price + "</li>");
    	if (the_type === "")
    	{
    		the_type = results.rows.item(i).type;
    		$("#menu_list").append("<div data-role='collapsible'><h2>"
					+ results.rows.item(i).type + 
					"</h2><ul id='menu_list" + results.rows.item(i).type +
					"' data-role='listview' data-theme='d'></ul></div>");
    	}
    	else if (the_type != results.rows.item(i).type){
    		the_type = results.rows.item(i).type;
    		$("#menu_list").append("<div data-role='collapsible'>  <h3>"
    						+ results.rows.item(i).type + 
    						"</h3><ul id='menu_list" + results.rows.item(i).type +
    						"' data-role='listview' data-theme='d'></ul></div>");
    	}
    	
    	$("#menu_list"+results.rows.item(i).type).append("<li><a class='item_click' id="+results.rows.item(i).name+","+results.rows.item(i).price+">" + results.rows.item(i).name + " -- " + results.rows.item(i).type + "</a></li>");
    }  
    $(".item_click").bind("tap", function() {
    	var tuple = $(this).attr('id');
    	name = tuple.split(",")[0];
    	
    	var pic_name = "no.png";
    	if (name === "Burger")
    	{ pic_name = "burger.png"; }
    	else if (name === "Coffee")
    	{ pic_name = "drink.png"; }
    	else if (name === "Donut")
    	{ pic_name = "donut.png"; }
    	else if (name === "Ice Cream")
    	{ pic_name = "icecream.png"; }
    	else if (name === "Salad")
    	{ pic_name = "salad.png"; }
    	
    	//$("#img").html("<img src=http://ipro315.dyndns.org/static"+ pic_name +" style='width: 100%;'/>");
    	$("#img").html("<img src=images/"+ pic_name +" style='width: 50%;'/>"); // local pic
    	
    	price = tuple.split(",")[1];
    	$("#item_name").text(name);
    	$("#item_price").text("$ " + price);
		$("#item_qty").text("1");
    	$.mobile.changePage("#itemPage");
    });
    
    $("#add_to_bag").bind("tap", function(){
    	var db = window.openDatabase("Database", "1.0", "ipro315", 200000);
    	
    	db.transaction(function(tx){
    		tx.executeSql('CREATE TABLE IF NOT EXISTS ORDERS (name, qty)');
    	});
    	
    	var name = $("#item_name").text();
    	var qty = $("#item_qty").text();
    	
    	if (mutex == 0)
    	{    	
    		mutex = 1;  		
    		// Delete the old row and add a new row.
    		alert("Item added");
	    	db.transaction(function(tx){
	    		tx.executeSql('DELETE FROM ORDERS WHERE name=?', [name]);
		        tx.executeSql('INSERT INTO ORDERS (name, qty) VALUES (?,?)', [name, qty]);
	    	});     		
	    	setTimeout("mutex = 0",500);
	    }
    });
}


function show_bag(){
	var db = window.openDatabase("Database", "1.0", "ipro315", 200000);
	db.transaction(function(tx){
		tx.executeSql('SELECT * FROM ORDERS', [], showBagSuccess, errorCB);
	});
}

function showBagSuccess(tx, results) {
	var len = results.rows.length;
	$("#bag_table").html("");
	//$("#bag_table").append("<tr><td style='width: 150px'><h2>Item Name</h2></td><td><h2>Quantity</h2></td></tr>");
	$("#bag_table").append('<thead><tr><th scope="col">Item Name:</th><th scope="col">Quantity:</th></tr></thead>');

	for (var i=0; i<len; i++) {
		$("#bag_table").append("<tr><td>"+results.rows.item(i).name+"</td><td>"+results.rows.item(i).qty +"</td><td><input class='order_data' type=hidden name="+results.rows.item(i).name+" value="+results.rows.item(i).qty+" /></td></tr>");
	}  		
    $("#clear_bag").bind("tap", clear_bag);
}

function clear_bag(){
	var db = window.openDatabase("Database", "1.0", "ipro315", 200000);
	
	if (mutex == 0)
	{   
		mutex = 1;
		db.transaction(function(tx){
			tx.executeSql('DROP TABLE IF EXISTS ORDERS');
		});
		$("#bag_table").html("<h3>Empty</h3>");
		$.mobile.changePage("#bagPage");
    	setTimeout("mutex = 0",400);
	}
}

function submit_order(){
	//var flag = proximity_test();   //this will return boolean
	//alert("submit button pressed");
	var count = 0;
	var dataString = "";
	$("input.order_data").each(function() {
		var key = $(this).attr('name');
		var value = $(this).val();
		if (count == 0){
            dataString = key + "=" + value;
            count = 1;
        }
        else{
            dataString = dataString + "&" + key + "=" + value;
        }
	});
		
	if (mutex == 0)
	{    	
		mutex = 1;  	
		getDevicesEvent();
	}
}

function pass_submit_order(){
	alert("Password Submit button ");
	var count = 0;
	var dataString = "";
	$("input.order_data").each(function() {
		var key = $(this).attr('name');
		var value = $(this).val();
		if (count == 0){
            dataString = key + "=" + value;
            count = 1;
        }
        else{
            dataString = dataString + "&" + key + "=" + value;
        }
	});
	
	var pass_value = $("#pass_value").val();
	dataString = dataString + "&pass=" + pass_value;
	
	if (mutex == 0)
	{    	
		mutex = 1;  	
		$.ajax({
	        type: "POST",
	        url: "http://ipro315.dyndns.org/simulator",
	        data: dataString,
	        success: function(data) {
				clear_bag(); 
	            alert(data);
	        },
	        error: function() {alert("Server Error");}
	    });
    	setTimeout("mutex = 0", 400);	
	}
}

function ajax_order()
{
	alert("Start ajax");
	$.ajax({
        type: "POST",
        url: "http://ipro315.dyndns.org/simulator",
	    data: dataString,
	    success: function() {
			clear_bag() //empty bag
	        alert("Order Submitted");
	    },
	    error: function() {alert("Server Error");}
	});
    setTimeout("mutex = 0",400);			
}


/**
 * Facilitates the call into the Bluetooth plugin's BTDeviceManager.
 * 
 * @param win - success callback
 * @param fail - error callback
 */
var getDevices = function(win, fail) {
	if (window['BTDeviceManager'] == undefined)
		PluginManager.addService("BTDeviceManager","com.phonegap.BluetoothPlugin.BTDeviceManager");
	PhoneGap.execAsync(win, fail, "BTDeviceManager", "getDevices", []);
};

/**
 * Displays the list of found devices in the user interface.
 * @param number - device list (JSON)
 */
function updateDeviceList(number) {
	var mac = " 00:26:4A:9C:B9:EE";
	var mac1 = "00:26:4A:9C:B9:EE ";
	var mac2 = "00:26:4A:9C:B9:EE";
	var mac3 = "7C:6D:62:A4:5F:D1"

	devices = number.split(",");
	for (d in devices) {
		if (devices[d] == mac || devices[d] == mac1 || devices[d] == mac2 || devices[d] == mac3)
		{
			//$("#test").text(devices[d]);
			alert(devices[d]);
			$.ajax({
		        type: "POST",
		        url: "http://ipro315.dyndns.org/simulator",
			    data: dataString,
			    success: function(data) {
					clear_bag() //empty bag
			        alert(data);
			    },
			    error: function() {alert("Server Error");}
			});
		    setTimeout("mutex = 0",400);	
		}
		$("#devices").append(devices[d]);
		$("#devices").append("<br/>");
	}
	$("#get-discoverable-devices").text('Find some devices!');
	$("#get-discoverable-devices").addClass('reallink');
	$("#get-discoverable-devices").removeClass('no_underline');
	$("#get-discoverable-devices").bind('click', getDevicesEvent);
}

/**
 * Event fired when user clicks the Get some devices link.
 */

function getDevicesEvent() {
	getDevices(
			function(r){
				// The call has been successfully made.
				$("#get-discoverable-devices").text('Gathering devices...');
				$("#get-discoverable-devices").unbind('click');
				$("#get-discoverable-devices").addClass('no_underline');
				$("#get-discoverable-devices").removeClass('reallink');
				//alert(r);
			},
			function(e){alert(e)}
	);
}
/* End of Bluetooth 
 * 
 */